import { createAppContainer, createSwitchNavigator } from 'react-navigation'
import Counter from './screens/Counter'
import Login from './screens/Login'
import Signup from './screens/Signup'

const SwitchNavigator = createSwitchNavigator(
  {
    Login: {
      screen: Login
    },
    Signup: {
      screen: Signup
    },
    Counter: {
      screen: Counter
    }
  },
  {
    initialRouteName: 'Login'
  }
)

export default createAppContainer(SwitchNavigator)