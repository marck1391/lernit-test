import { Component } from 'react'

export default class CustomComponent extends Component {
  _isMounted = false

  constructor(props) {
    super(props)
    this._oldSetState = this.setState.bind(this)
    this.setState = this._setState
    this.render = this.render.bind(this)
  }

  _setState(...args) {
    if (this._isMounted) {
      this._oldSetState(...args)
    }
  }

  componentDidMount() {
    this._isMounted = true
  }

  componentWillUnmount() {
    this._isMounted = false
  }

  render() { return null }
}