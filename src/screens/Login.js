import auth from '@react-native-firebase/auth'
import React, { Component } from 'react'
import { AsyncStorage, Button, StyleSheet, Text, TextInput, View } from 'react-native'

export default class Login extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      error: null,
      loaded: false
    }
  }

  componentDidMount() {
    AsyncStorage.getItem('email').then((email) => {
      if (email) {
        this.props.navigation.navigate('Counter')
        return
      }
      this.setState({ loaded: true })
    }).catch(e => {
      this.setState({ loaded: true })
    })
  }

  onChangeText = (field, value) => {
    this.setState({
      [field]: value
    })
  }

  login = () => {
    this.setError()
    let { email, password } = this.state
    if (!email || !password)
      return this.setError('Porfavor llena todos los campos')
    auth()
      .signInWithEmailAndPassword(email, password)
      .then(async () => {
        await AsyncStorage.setItem('email', email)
        this.props.navigation.navigate('Counter')
      })
      .catch(error => {
        console.log('INVALID USER', error)
        let errorMsg = 'Error desconocido, porfavor intenta mas tarde'
        if (error.message.includes('wrong-password') || error.message.includes('user-not-found'))
          errorMsg = 'Correo o contraseña invalidos'
        else if (error.message.includes('invalid-email'))
          errorMsg = 'Correo invalido'
        else if (error.message.includes('blocked'))
          errorMsg = 'Demasiados intentos para iniciar sesion. Porfavor intenta mas tarde.'
        this.setError(errorMsg)
      })
  }

  signup = () => {
    this.props.navigation.navigate('Signup')
  }

  setError(errormsg) {
    if (!errormsg) this.setState({ error: null })
    this.setState({
      error: <Text style={styles.error}>{errormsg}</Text>
    })
  }

  render() {
    if (!this.state.loaded) {
      return null
    }
    return (
      <View>
        <Text style={styles.title}>Lernit Test</Text>
        {this.state.error}
        <TextInput
          style={styles.textInput}
          onChangeText={this.onChangeText.bind(this, 'email')}
          placeholder="Email"
          placeholderTextColor="white"
          value={this.state.email} />
        <TextInput
          style={styles.textInput}
          onChangeText={this.onChangeText.bind(this, 'password')}
          placeholder="Contraseña"
          placeholderTextColor="white"
          secureTextEntry={true}
          value={this.state.password} />
        <Button title="Entrar" onPress={this.login} />
        <View style={{ marginBottom: 10 }}></View>
        <Button title="Crear cuenta" onPress={this.signup} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: 24,
    marginTop: 30,
    marginBottom: 30
  },
  textInput: {
    borderColor: 'white',
    color: 'white',
    borderWidth: 1,
    padding: 10,
    height: 40,
    fontSize: 16,
    margin: 10
  },
  error: {
    color: 'red',
    fontSize: 16,
    textAlign: 'center'
  }
})