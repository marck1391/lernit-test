import auth from '@react-native-firebase/auth'
import React, { Component } from 'react'
import { Button, StyleSheet, Text, TextInput, View } from 'react-native'

export default class Signup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      password: '',
      confirmpassword: '',
      error: null
    }
  }

  onChangeText = (field, value) => {
    this.setState({
      [field]: value
    })
  }

  signup = () => {
    this.setError()
    let { email, password, confirmpassword } = this.state
    if (!email || !password || !confirmpassword)
      return this.setError('Porfavor llena todos los campos')
    if (password !== confirmpassword)
      return this.setError('Las contraseñas no coinciden')
    auth()
      .createUserWithEmailAndPassword(email, password)
      .then(() => {
        this.props.navigation.navigate('Counter')
      })
      .catch(error => {
        console.log('ERROR_SIGNUP', error)
        let errorMsg = error.message.replace(/\[[^\[]*\](.*)/, '$1')
        if (error.message.includes('email-already-in-use'))
          errorMsg = 'El correo esta actualmente en uso'
        else if (error.message.includes('invalid-email'))
          errorMsg = 'Correo invalido'
        this.setError(errorMsg)
      })
  }

  setError(errormsg) {
    this.setState({
      error: <Text style={styles.error}>{errormsg}</Text>
    })
  }

  login = () => {
    this.props.navigation.navigate('Login')
  }

  render() {
    return (
      <View>
        <Text style={styles.title}>Lernit Test</Text>
        {this.state.error}
        <TextInput
          style={styles.textInput}
          onChangeText={this.onChangeText.bind(this, 'email')}
          placeholder="Email"
          placeholderTextColor="white"
          value={this.state.email} />
        <TextInput
          style={styles.textInput}
          onChangeText={this.onChangeText.bind(this, 'password')}
          placeholder="Contraseña"
          placeholderTextColor="white"
          secureTextEntry={true}
          value={this.state.password} />
        <TextInput
          style={styles.textInput}
          onChangeText={this.onChangeText.bind(this, 'confirmpassword')}
          placeholder="Confirmar contraseña"
          placeholderTextColor="white"
          secureTextEntry={true}
          value={this.state.confirmpassword} />
        <Button title="Registrarse" onPress={this.signup} />
        <View style={{ marginBottom: 10 }}></View>
        <Button title="Entrar" onPress={this.login} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  title: {
    textAlign: 'center',
    color: 'white',
    fontSize: 24,
    marginTop: 30,
    marginBottom: 30
  },
  textInput: {
    borderColor: 'white',
    color: 'white',
    borderWidth: 1,
    padding: 10,
    height: 40,
    fontSize: 16,
    margin: 10
  },
  error: {
    color: 'red',
    fontSize: 16,
    textAlign: 'center'
  }
})