import auth from '@react-native-firebase/auth'
import firestore from '@react-native-firebase/firestore'
import React from 'react'
import { AsyncStorage, Button, StyleSheet, Text, View } from 'react-native'
import Component from '../CustomComponent'

export default class Counter extends Component {
  constructor(props) {
    super(props)
    this.state = {
      email: '',
      loaded: false,
      count: 0
    }
    this.load()
  }

  async load() {
    let email = await AsyncStorage.getItem('email').catch(e => null)
    if (!email) return this.logout()
    this.setState({ email })
    await this.getCounter(email)
    const counterRef = await firestore().collection('counters').doc(email)
    counterRef.onSnapshot(doc => {
      //qs.forEach(doc => {
      let data = doc.data()
      console.log('DOC', data)
      let { count } = data
      this.setState({ count })
      //})

    }, err => {
      console.log('ERROR ONSNAP', err)
    })
  }

  onChangeText = (field, value) => {
    this.setState({
      [field]: value
    })
  }


  getCounter = async (email) => {
    if (!email) return
    const counterRef = await firestore().collection('counters')
    let counter = await counterRef.doc(email).get()
      .then(snap => {
        if (!snap.exists) {
          let counter = counterRef.doc(email).set({
            count: 0
          })
          return { count: 0 }
        }
        return snap.data()
      }).catch(err => {
        console.log('Error', err)
        return null
      })
    if (counter == undefined) return console.log('ERROR COUNTER NOT FOUND')
    this.setState({
      count: counter.count,
      loaded: true
    })
  }

  increment = async () => {
    console.log('increment')
    const counterRef = firestore().collection('counters')
    return counterRef.doc(this.state.email).update({
      count: this.state.count + 1
    }).then(() => {
      console.log('Updated')
      return true
    }).catch(e => {
      console.log('Error, could not update', e)
      return false
    })
  }

  logout = async () => {
    await AsyncStorage.removeItem('email')
    await auth().signOut()
    this.props.navigation.navigate('Login')
  }

  render() {
    if (!this.state.loaded) return null
    return (
      <View style={styles.whiteBg}>
        <Text style={styles.countText}>{this.state.count}</Text>
        <Button title="Aumentar" onPress={this.increment} />
        <View style={{ marginBottom: 10 }}></View>
        <Button title="Cerrar sesion" onPress={this.logout} />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  whiteBg: {
    flex: 1,
    backgroundColor: 'white',
    padding: 10
  },
  countText: {
    fontSize: 124,
    textAlign: 'center'
  }
})