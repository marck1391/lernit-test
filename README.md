# Lernit Test

### Installation
Prerequisites:
1. NPM ~v6.13.14
2. Node ~v12.14.0
3. Android Device/Emulator
4. Android Development environment

```bash
git clone https://marck1391@bitbucket.org/marck1391/lernit-test.git
cd lernit-test
npm install
```

### Run
```bash
npx react-native run-android
```
In case of failure:
```bash
rm -rf android/app/build/intermediates/signing_config/debug/out/signing-config.json
cd android && ./gradlew clean && ./gradlew :app:bundleRelease
npx react-native run-android
```